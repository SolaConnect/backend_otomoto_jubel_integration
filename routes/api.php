<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('accesskey')->prefix('/v1.0')->group(function() {
Route::group([
    'middleware' => 'accesskey',
    'prefix' => 'v1.0'
], 

function() {
    Route::post('/order', 'OrderController@add');
    Route::post('/order/status', 'OrderController@update');
});
