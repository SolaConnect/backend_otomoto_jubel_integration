<?php

namespace App\Partners;

use App\Enums\UnitStatus;
use App\Order;
use App\OrderListing;
use App\Unit;
use App\UnitListing;
use Carbon\Carbon;
use Exception;
use GuzzleHttp\Client;
use Log;
use Webpatser\Uuid\Uuid;

class Momotor {

    protected $client;
    public $token;
    protected $url;
    protected $unit;
    protected $unitType;
    protected $unitListing;
    protected $order;
    protected $orderListing;

    public function __construct()
    {
        $this->client = new Client();
        $this->setToken();
    }

    private function buildUrl($endpoint)
    {
        $this->url = env('MOMOTOR_INTEGRATION_URL') . $endpoint;
        return ;
    }

    private function callAPI($payload)
    {
        $res = $this->client->request(
            'POST',
            $this->url, 
            [
                'headers' => [
                    'Content-Type'  => 'application/json',
                    'User-Agent'    => 'OTOMOTO',
                    'Authorization' => ($this->token['status'] == 'success') ? 'Bearer '.$this->token['token'] : ''
                ],
                'body' => json_encode($payload)
            ]
        )->getBody();

        return json_decode($res);
    }

    public function setToken()
    {
        $this->buildUrl('/otomoto/token');

        try {
            $payload = [
                'email'    => env('MOMOTOR_TOKEN_EMAIL'),
                'password' => env('MOMOTOR_TOKEN_PASSWORD')
            ];

            $res = $this->callAPI($payload);

            if ($res->status_code != 200) {
                $token = [
                    'status'  => 'failed',
                    'code'    => 400,
                    'message' => 'Failed to get token'
                ];
            } else {
                $token = [
                    'status'  => 'success',
                    'code'    => 200,
                    'message' => 'success',
                    'token'   => $res->token
                ];
            }
        } catch (Exception $e) {
            Log::error($e->getFile().' line:'.$e->getLine().' message: '.$e->getMessage());
            $token = [
                'status'  => 'failed',
                'code'    => 500,
                'message' => "Failed to get token, reason : ".$e->getMessage()
            ];
        }

        $this->token = $token;

        return ;
    }


    /**
     *  MOMOTOR - UNIT
     */

    public function setUnit($unit)
    {
        $this->unit = $unit;
        $this->setUnitListing();

        return ;
    }

    public function getUnit()
    {
        return $this->unit;
    }

    public function setUnitByUnitListingId($id)
    {
        $unitListing = UnitListing::where('UNIT_LISTING_ID', $id)->first();

        if ($unitListing) {
            $unit = Unit::where('UNIT_ID', $unitListing->UNIT_ID)->first();
            
            $this->unit        = $unit;
            $this->unitListing = $unitListing;
            
            return true;
        }

        return false;
    }

    public function setUnitType()
    {
        $unitType = VehicleUnitType::where('VM_ID', $this->unit->VM_ID)->whereNull('DELETE_BY')->first();
        
        if ($unitType) {
            $this->unitType = $unitType;
            
            return true;
        }

        return false;
    }

    public function isUnitSold()
    {
        return $this->unit->STATUS == UnitStatus::SOLD;
    }

    private function generateUnitListing()
    {
        $unitListingId = Uuid::generate(4)->string;

        $unitListing                  = new UnitListing;
        $unitListing->UNIT_ID         = $this->unit->UNIT_ID;
        $unitListing->UNIT_LISTING_ID = $unitListingId;
        $unitListing->FOREIGN_UNIT_ID = '';
        $unitListing->CHANNEL         = 'MOMOTOR';

        $this->unitListing = $unitListing;

        return $unitListingId;
    }

    private function setUnitListing()
    {
        $this->unitListing = UnitListing::where('UNIT_ID', $this->unit->UNIT_ID)->first();

        return ;
    }

    public function updateUnit()
    {
        $updated = false;

        $this->buildUrl('/otomoto/order/status');

        if (!$this->unitListing) {
            return $updated;
        }

        try {
            $payload = [
                'listing_id' => $this->unitListing->FOREIGN_UNIT_ID,
                'status'     => strtoupper($this->unit->STATUS)
            ];

            $res = $this->callAPI($payload);

            if ($res->status_code == 200) {
                $updated = true;
            }
        } catch (Exception $e) {
            Log::error($e->getFile().' line:'.$e->getLine().' message: '.$e->getMessage());
        }

        return $updated;
    }

    /**
     *  MOMOTOR - ORDER
     */

    public function setOrder($order)
    {
        $this->order = $order;
        $this->setOrderListing();

        return ;
    }

    public function resetOrder()
    {
        $this->order = null;

        return ;
    }

    private function setOrderListing()
    {
        $this->orderListing = OrderListing::where('ORDER_ID', $this->order->ORDER_ID)->first();

        return ;
    }

    public function generateOrderListing($foreignOrderId)
    {
        $orderListingId = Uuid::generate(4)->string;

        $orderListing                   = new OrderListing;
        $orderListing->ORDER_ID         = $this->order->ORDER_ID;
        $orderListing->ORDER_LISTING_ID = $orderListingId;
        $orderListing->FOREIGN_ORDER_ID = $foreignOrderId;
        $orderListing->CREATED_BY       = 'MOMOTOR';
        $orderListing->save();

        return $orderListingId;
    }

    public function updateOrder()
    {
        $updated = false;

        $this->buildUrl('/otomoto/order/status');

        if (!$this->orderListing) {
            return $updated;
        }

        try {
            $payload = [
                'order_id' => $this->orderListing->FOREIGN_ORDER_ID,
                'status'   => strtoupper($this->order->STATUS)
            ];

            $res = $this->callAPI($payload);

            if ($res->status_code == 200) {
                $updated = true;
            }
        } catch (Exception $e) {
            Log::error($e->getFile().' line:'.$e->getLine().' message: '.$e->getMessage());
        }

        return $updated;
    }

}