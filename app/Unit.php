<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Unit extends Model
{
    protected $table = 'tr_jubel_unit';
    protected $primaryKey = 'UNIT_ID';
    public $incrementing = false;

    public function unit_listing()
    {
        return $this->hasMany(UnitListing::class, 'UNIT_ID');
    }
}
