<?php

namespace App\Http\Middleware;

use Closure;

use App\ThirdPartyAccessKey;

class CheckAccessKey
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $thirdPartyName = $request->header('User-Agent');
        $thirdPartyKey  = $request->header('Token');

        $thirdParty = ThirdPartyAccessKey::where('NAME', $thirdPartyName)->where('ACCESS_KEY', $thirdPartyKey)->where('IS_ACTIVE', 1)->first();

        if (!$thirdParty) {
            $response = [
                'CODE' => 401,
                'MESSAGE' => 'Invalid access key'
            ];

            return response()->json($response, 401);
        }

        return $next($request);
    }
}
