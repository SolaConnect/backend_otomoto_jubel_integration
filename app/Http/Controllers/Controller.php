<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function getMomotorToken()
    {
        $client = new Client();

        $url = env('MOMOTOR_INTEGRATION_URL').'/otomoto/token';

        try {
            $payload = [
                'email' => env('MOMOTOR_TOKEN_EMAIL'),
                'password' => env('MOMOTOR_TOKEN_PASSWORD')
            ];

            $res = $client->post($url, [
                'header' => [
                    'Content-Type' => 'application/json',
                    'User-Agent' => 'OTOMOTO'
                ],
                'body' => json_encode($payload)
            ])->getBody()->getContents();

            $res = json_decode($res);

            if ($res->code != 200) {
                $response = [
                    'code' => 400,
                    'message' => 'Failed to get token'
                ];
            } else {
                $response = [
                    'code' => 200,
                    'message' => 'success',
                    'token' => $res->data->token
                ];
            }
        } catch (\Exception $e) {
            $response = [
                'code' => 400,
                'message' => "Failed to get token"
            ];
        }

        return $response;
    }
}
