<?php

namespace App\Http\Controllers;

use App\Enums\OrderStatus;
use App\Enums\UnitStatus;
use App\Order;
use App\OrderListing;
use App\Partners\Momotor;
use App\Unit;
use App\UnitListing;
use DB;
use Exception;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Log;
use Webpatser\Uuid\Uuid;

class OrderController extends Controller
{
    protected $response;

    public function __construct() {
        $this->response = [
            'CODE'    => 200,
            'MESSAGE' => ''
        ];
    }

    public function add(Request $request) {
        try {
            $unitListingId = $request->json('UNIT_LISTING_ID');

            $momotor = new Momotor();

            if ($momotor->setUnitByUnitListingId($unitListingId)) {
                // reject if unit status is SOLD
                if ($momotor->isUnitSold()) {
                    return response()->json([
                        'CODE'    => 500,
                        'MESSAGE' => 'Unit already sold'
                    ], 500);
                }

                $order = OrderListing::where('FOREIGN_ORDER_ID', $request->json('BUY_REQUEST_ID'))->first();
                if ($order) {
                    return response()->json([
                        'CODE'    => 500,
                        'MESSAGE' => 'Duplicate Buy Request ID'
                    ], 500);
                }

                if (!OrderStatus::hasKey($request->json('STATUS'))) {
                    return response()->json([
                        'CODE'    => 400,
                        'MESSAGE' => 'Invalid buy request status'
                    ], 400);
                }

                DB::beginTransaction();

                $unit = $momotor->getUnit();
                
                // Create Order object data
                $order               = new Order;
                $order->ORDER_ID     = Uuid::generate(4);
                $order->UNIT_ID      = $unit->UNIT_ID;
                $order->NAME         = $request->json('NAME');
                $order->EMAIL        = $request->json('EMAIL');
                $order->PHONE        = $request->json('PHONE');
                $order->PRICE        = $request->json('PRICE');
                $order->PAYMENT_TYPE = strtolower($request->json('PAYMENT_TYPE'));
                $order->STATUS       = strtolower($request->json('STATUS'));
                $order->save();

                $momotor->setOrder($order);

                // Create Order Listing data
                $orderListingId = $momotor->generateOrderListing($request->json('BUY_REQUEST_ID'));

                DB::commit();

                $statusCode = 200;
                $message = 'Success';

                $this->response['DATA'] = [
                    'ORDER_ID' => $orderListingId
                ];
            } else {
                $statusCode = 404;
                $message = 'Unit not found';
            }
        } catch (Exception $e) {
            DB::rollback();
            Log::error($e->getFile().' line:'.$e->getLine().' message: '.$e->getMessage());
            $statusCode = 500;
            $message = 'Error';
        }

        $this->response['CODE']    = $statusCode;
        $this->response['MESSAGE'] = $message;

        return response()->json($this->response, $statusCode);
    }

    public function update(Request $request) {
        try {
            $orderListingId = $request->json('ORDER_ID');
            $orderListing   = OrderListing::where('ORDER_LISTING_ID', $orderListingId)->first();
            
            if ($orderListing) {
                if (!OrderStatus::hasKey($request->json('STATUS'))) {
                    return response()->json([
                        'CODE'    => 400,
                        'MESSAGE' => 'Invalid buy request status'
                    ], 400);
                }

                $order = Order::where('ORDER_ID', $orderListing->ORDER_ID)->first();
                $unit  = Unit::find($order->UNIT_ID);

                /*if ($unit->STATUS == UnitStatus::SOLD) {
                    return response()->json([
                        'CODE'    => 400,
                        'MESSAGE' => 'Unit already sold'
                    ], 400);
                }*/

                DB::beginTransaction();

                $order->STATUS = strtolower($request->json('STATUS'));

                // Check if status updated to DEAL
                if ($request->json('STATUS') == 'DEAL') {
                    $momotor = new Momotor();

                    if ($momotor->token['status'] == 'failed') {
                        return response()->json([
                            'CODE'    => 500,
                            'MESSAGE' => 'Failed to get Token'
                        ], 500);
                    }

                    // Update Unit status to SOLD
                    $unit->STATUS = UnitStatus::SOLD;
                    $unit->save();
                    
                    $momotor->setUnit($unit);

                    if (!$momotor->updateUnit()) {
                        Log::error("Failed to update Unit #$unit->UNIT_ID to Momotor");
                    }

                    $orders = Order::where('UNIT_ID', $order->UNIT_ID)->whereIn('STATUS', [OrderStatus::OPEN, OrderStatus::PROCESSING])->get();

                    // Update other Orders from same unit to CLOSED
                    foreach ($orders as $key => $temp) {
                        $temp->STATUS = OrderStatus::CLOSED;
                        $temp->save();

                        $momotor->setOrder($temp);
                        
                        if (!$momotor->updateOrder()) {
                            Log::error("Failed to update Order #$temp->ORDER_ID to Momotor");
                        }
                        
                        $momotor->resetOrder();
                    }
                }

                $order->save();

                DB::commit();

                $statusCode = 200;
                $message = 'Success';
            } else {
                $statusCode = 404;
                $message = 'Buy request not found';
            }
        } catch (Exception $e) {
            DB::rollback();
            Log::error($e->getFile().' line:'.$e->getLine().' message: '.$e->getMessage());
            $statusCode = 500;
            $message = 'Error';
        }
    
        $this->response['CODE'] = $statusCode;
        $this->response['MESSAGE'] = $message;

        return response()->json($this->response, $statusCode);
    }
}
