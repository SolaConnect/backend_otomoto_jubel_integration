<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = 'tr_jubel_order';
    protected $primaryKey = 'ORDER_ID';
    public $incrementing = false;

    public function order_listing()
    {
        return $this->hasMany(OrderListing::class, 'ORDER_ID');
    }
}
