<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderListing extends Model
{
    protected $table = 'tr_jubel_order_listing';
    protected $primaryKey = 'ORDER_ID';
    public $incrementing = false;
}
