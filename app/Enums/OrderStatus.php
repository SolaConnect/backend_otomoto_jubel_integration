<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

final class OrderStatus extends Enum
{
    const OPEN = "open";
    const PROCESSING = "processing";
    const CANCELED = "canceled";
    const DEAL = "deal";
    const CLOSED = "closed";
}
