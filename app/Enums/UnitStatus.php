<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

final class UnitStatus extends Enum
{
    const OPEN = "open";
    const SOLD = "sold";
}
