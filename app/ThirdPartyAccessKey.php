<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ThirdPartyAccessKey extends Model
{
    protected $table = 'tm_jubel_third_party_access_key';
    protected $primaryKey = 'ID';
}
