<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UnitListing extends Model
{
    protected $table = 'tr_jubel_unit_listing';
    protected $primaryKey = 'UNIT_ID';
    public $incrementing = false;
}
